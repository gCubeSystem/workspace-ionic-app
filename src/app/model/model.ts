/*export interface Items {
    itemlist: Item[];
}*/

export interface AbtractItem {
    "@class":             string;
    content?:             Content;
    id:                   string;
    name:                 string;
    path:                 string;
    parentId:             string;
    parentPath:           String;
    primaryType:          String;
    trashed:              boolean;
    externalManaged:      boolean;
    shared:               boolean;
    locked:               boolean;
    publicItem:           boolean;
    title:                string;
    description:          string;
    lastModifiedBy:       string;
    lastModificationTime: number;
    creationTime:         number;
    lastAction:           LastAction;
    hidden:               boolean;
    accounting:           null;
    externalRoot?:        boolean;
    privilege?:           null;
    vreFolder?:           boolean;
    displayName?:         string;
    
}

export interface AbstractItem {
    content?:             Content;
    id:                   string;
    name:                 string;
    path:                 string;
    parentId:             string;
    parentPath:           String;
    primaryType:          String;
    trashed:              boolean;
    externalManaged:      boolean;
    shared:               boolean;
    locked:               boolean;
    publicItem:           boolean;
    title:                string;
    description:          string;
    lastModifiedBy:       string;
    lastModificationTime: number;
    creationTime:         number;
    lastAction:           LastAction;
    hidden:               boolean;
    accounting:           null;
    externalRoot?:        boolean;
    privilege?:           null;
    vreFolder?:           boolean;
    displayName?:         string;
    
}

export interface Content {
    size:             number;
    data:             string;
    remotePath:       string;
    mimeType:         string;
    storageId:        string;
    managedBy:        String;
    numberOfPages?:   number;
    version?:         string;
    author?:          string;
    title?:           string;
    producer?:        string;
    width?:           number;
    height?:          number;
    thumbnailWidth?:  number;
    thumbnailHeight?: number;
    thumbnailData?:   Blob;
}

export enum LastAction {
    Cloned = "CLONED",
    Created = "CREATED",
    Moved = "MOVED",
    Renamed = "RENAMED",
    Updated = "UPDATED",
}

