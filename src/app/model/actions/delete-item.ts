import { AlertOptions } from "@ionic/angular";
import { Observable } from "rxjs";
import { StoragehubService } from "src/app/storagehub.service";
import { WSItem } from "../ws-item";
import { Action } from "./action";

export class DeleteAction extends Action {

    override getAlertOptions(item: WSItem, storagehub: StoragehubService, reload: Function): AlertOptions {
        var title = item.getTitle();
        var options: AlertOptions = {
            header: `Are you sure to delete item ${title} ?`,
            buttons: [{
                text: 'No',
                role: 'cancel'
            },
            {
                text: 'Yes',
                handler: () => {
                    this.actionHandler(item, storagehub).then((obs) => obs.subscribe(
                        () => reload(item.item.parentId)
                    ));
                }
            }]
        };
        return options;

    }
    actionHandler(data: WSItem, storagehub: StoragehubService) {
        return storagehub.deleteItem(data.item.id);
    }

    getName(): string {
        return "Delete";
    }
    getActionType(): string | undefined {
        return "destructive";
    }

}