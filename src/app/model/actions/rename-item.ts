import { AlertOptions } from "@ionic/angular";
import { Observable } from "rxjs";
import { StoragehubService } from "src/app/storagehub.service";
import { WSItem } from "../ws-item";
import { Action } from "./action";

export class RenameAction extends Action {

    override getAlertOptions(item: WSItem, storagehub: StoragehubService, reload: Function): AlertOptions {
        var title = item.getTitle();
        var options: AlertOptions = {
            header: 'Rename Item',
            message: 'Please specify the new name',
            inputs: [
              {
                name: 'name',
                type: 'text',
                placeholder: item.getTitle()
              }
            ],
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Rename',
                handler: (data) => {
                    this.actionHandler({item: item, newName: data.name}, storagehub).then( (obs) => obs.subscribe(
                        () => reload(item.item.parentId)
                    ));
                }
              }
            ]
          };
        return options;

    }
    actionHandler(data: {item: WSItem, newName : string}, storagehub: StoragehubService) {
        return storagehub.renameItem(data.item.item.id, data.newName);
    }

    getName(): string {
        return "Rename";
    }
    getActionType(): string | undefined {
        return undefined;
    }
}
