import { ModalOptions } from "@ionic/angular";
import { Observable } from "rxjs";
import { StoragehubService } from "src/app/storagehub.service";
import { WsViewerComponent } from "src/app/ws-viewer/ws-viewer.component";
import { WSItem } from "../ws-item";
import { Action } from "./action";

export class CopyAction extends Action {


    override getModalOptions(item: WSItem, storagehub: StoragehubService, reload: Function, notify: Function): ModalOptions | undefined {
        return {
            component: WsViewerComponent,
            componentProps: {
                finishLabel: "Copy here",
                title: "Select destination folder for " + item.getTitle(),
                notClickableIds: [item.item.id],
                notSelectableIds: [item.item.parentId],
                onSelected: (destinationItem: WSItem) => {
                    this.actionHandler({ item: item, destinationItem: destinationItem }, storagehub).then( (obs) => obs.subscribe(
                        () => {
                            reload(destinationItem.item.id)
                            notify(`${item.getTitle()} copied`);
                        }
                    ))
                }
            }
        };
    }


    actionHandler(data: { item: WSItem, destinationItem: WSItem }, storagehub: StoragehubService){
        return storagehub.copyItem(data.destinationItem.item.id, data.item.item.id, data.item.item.name);
    }

    getName(): string {
        return "Copy";
    }
    getActionType(): string | undefined {
        return undefined;
    }
}
