import { LoadingController, ModalOptions } from "@ionic/angular";
import { StoragehubService } from "src/app/storagehub.service";
import { WsViewerComponent } from "src/app/ws-viewer/ws-viewer.component";
import { WSItem } from "../ws-item";
import { Filesystem } from "@capacitor/filesystem";
import { UploaderInfoService } from "src/app/uploader-info.service";

export class UploadFile {

   

    getModalOptions(storagehub: StoragehubService, uploaderInfo: UploaderInfoService, loading: LoadingController, data: any, reload: Function, notify: Function, onSelectionFinished: Function): ModalOptions  {
        return {
            component: WsViewerComponent,
            componentProps: {
                finishLabel: "Upload here",
                title: "Select destination folder",
                notClickableIds: [],
                notSelectableIds: [],
                onSelected: (destinationItem: WSItem) => {
                    const itemId = destinationItem.item.id;
                    
                    this.showLoading(loading);
                    uploaderInfo.uploadStarted(itemId, data.title);
                    this.actionHandler(destinationItem, data, storagehub).then( obs => obs.subscribe({  
                            next: () => {}, 
                            error: err => { 
                                uploaderInfo.uploadFinished(itemId, data.title);
                                notify(`error uploading file ${data.title} to workspace`);
                            },
                            complete: () => {
                               notify(`uploaded file ${data.title}`);
                               uploaderInfo.uploadFinished(itemId, data.title),
                               loading.dismiss();
                               reload(itemId);
                               onSelectionFinished();
                            }, 
                            
                        }
                    ));
                }
            }
        };
    }


    async showLoading(loadingCtrl: LoadingController) {
        const loading = await loadingCtrl.create({
            message: 'Uploading...',
            duration: 100000,
            spinner: 'circles',
        });

        loading.present();
        return loading;
    }

    async actionHandler(destinationItem: WSItem,  result: any, storagehub: StoragehubService) {
        const pathDecodedWebPath = decodeURIComponent(result.webPath);
          
        const file = await fetch(pathDecodedWebPath)
        .then(res => res.blob()) // Gets the response and returns it as a blob
        .then(blob => { return new File([blob], result.title)});
        
        console.log("before uploading file ");
        return storagehub.uploadFile(destinationItem.item.id, result.title, file);
    }

    getName(): string {
        return "UploadFile";
    }
    getActionType(): string | undefined {
        return undefined;
    }
}