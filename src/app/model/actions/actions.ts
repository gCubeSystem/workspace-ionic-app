import { Action } from "./action";
import { CopyAction } from "./copy-item";
import { DeleteAction } from "./delete-item";
import { MoveAction } from "./move-item";
import { RenameAction } from "./rename-item";

export class Actions {

    static mv = new MoveAction();
    static cp = new CopyAction();
    static ren = new RenameAction();
    static del = new DeleteAction();

    public static getActionsPerType(type: string): Action[] {
        switch (type) {
            case 'SharedFolder':
                return [];
            case 'FolderItem':
                return [this.mv, this.ren, this.del];
            case 'TrashItem':
                return [this.del];
            case 'PDFFileItem':
                return [this.mv, this.cp, this.ren, this.del];
            case 'ImageFile':
                return [this.mv, this.cp, this.ren, this.del];
            case 'ExternalLink':
                return [this.mv, this.cp, this.ren, this.del];
            default:
                return [this.mv, this.cp, this.ren, this.del];
        }
        
    }
}

