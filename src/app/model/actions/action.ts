import { AlertOptions, ModalOptions } from "@ionic/angular";
import { Observable } from "rxjs";
import { StoragehubService } from "src/app/storagehub.service";
import { WSItem } from "../ws-item";

export abstract class Action {

    getAlertOptions(item: WSItem, storagehub: StoragehubService, postOp?: Function, notify?: Function): AlertOptions | undefined{
        return undefined;
    }

    getModalOptions(item: WSItem, storagehub: StoragehubService, postOp?: Function, notify?: Function): ModalOptions | undefined{
        return undefined;
    }

    abstract actionHandler(data: any, storagehub: StoragehubService): any;

    abstract getName(): string;

    abstract getActionType(): string | undefined;
}