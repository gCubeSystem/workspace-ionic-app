import { AlertOptions, ModalOptions } from "@ionic/angular";
import { Observable } from "rxjs";
import { StoragehubService } from "src/app/storagehub.service";
import { WSItem } from "../ws-item";
import { Action } from "./action";

export class CreateFolderAction extends Action {

  override getAlertOptions(item: WSItem, storagehub: StoragehubService, reload: Function): AlertOptions {
    var title = item.getTitle();
    var options: AlertOptions = {
      header: 'Create folder',
      message: 'Please specify the name of the new folder',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'new folder'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Create',
          handler: (data) => {
            this.actionHandler({ wsitem: item, name: data.name }, storagehub).then((obs) => obs.subscribe(
              () => reload(item.item.id)
            )
            );
          }
        }
      ]
    };
    return options;

  }
  actionHandler(data: { wsitem: WSItem, name: string }, storagehub: StoragehubService) {
    return storagehub.createFolder(data.wsitem.item.id, data.name, "");
  }

  getName(): string {
    return "Create Folder";
  }
  getActionType(): string | undefined {
    return undefined;
  }

}
