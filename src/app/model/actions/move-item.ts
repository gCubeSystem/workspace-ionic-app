import { ModalOptions } from "@ionic/angular";
import { StoragehubService } from "src/app/storagehub.service";
import { WsViewerComponent } from "src/app/ws-viewer/ws-viewer.component";
import { WSItem } from "../ws-item";
import { Action } from "./action";

export class MoveAction extends Action {

    
    override getModalOptions(item: WSItem, storagehub: StoragehubService, reload: Function, notify: Function): ModalOptions | undefined {
        return {
            component: WsViewerComponent,
            componentProps: {
                finishLabel: "Move here",
                title: "Select destination folder for "+item.getTitle(),
                notClickableIds: [item.item.id],
                notSelectableIds: [item.item.parentId],
                onSelected: (destinationItem:WSItem) =>{
                    this.actionHandler({item: item, destinationItem: destinationItem}, storagehub).then( (obs) => obs.subscribe(
                        () => {
                            reload(destinationItem.item.id);
                            notify(`${item.getTitle()} moved`);
                        }
                    ))
                }
            }
          };
    }

    
    actionHandler(data: {item: WSItem, destinationItem : WSItem}, storagehub: StoragehubService) {
        return storagehub.moveItem(data.destinationItem.item.id, data.item.item.id);
    }

    getName(): string {
        return "Move";
    }
    getActionType(): string | undefined {
        return undefined;
    }
}