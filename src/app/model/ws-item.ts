import { Item } from "./item.model";

export class WSItem {

    item: Item;
    type: string;

    temporary: boolean =false;

    constructor(item: Item) {
        this.item = item;
        const tempType$ = this.getType(item); 
        this.type = tempType$ ? tempType$ :""; 
    }

    getIconInfo(): string {
        let retrievedName: string;
        switch (this.type) {
            case 'SharedFolder':
                retrievedName = this.item ? "folder_special" : "folder_shared";
                break;
            case 'FolderItem':
                retrievedName = "folder";
                break;
            case 'TrashItem':
                retrievedName = "delete";
                break;
            case 'PDFFileItem':
                retrievedName = "picture_as_pdf";
                break;
            case 'ImageFile':
                retrievedName = "image";
                break;
            case 'ExternalLink':
                retrievedName = 'link';
                break;
            default:
                retrievedName = "description";
        }
        return retrievedName;
    }

   private  getType(item: Item): string | undefined {
        return item["@class"].split(".").pop();
    }

    isFolder(): boolean {
        return this.type === 'SharedFolder' ||
            this.type === 'ExternalFolder' ||
            this.type === 'FolderItem';
    }

    isFile(): boolean {
        return this.type === 'PDFFileItem' ||
            this.type === 'ImageFile' ||
            this.type === 'GenericFileItem';
    }

    getTitle(): string{  
       if (this.item.displayName && this.item.vreFolder)
            return this.item.displayName;
       else return this.item.title;
    }

}


