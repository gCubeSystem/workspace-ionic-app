import { WSItem } from "./ws-item";

export enum SortName {
    Name = "Name",
    LastModificationTime = "Last Modified",
    Size = "Size",
    FolderFirst = "Folders First"
}

export enum SortType {
    Asc = "arrow_upward",
    Desc = "arrow_downward"
}

export class Sorting {

    private static sortByNameASC = (item1: WSItem, item2: WSItem) => item1.getTitle().toLowerCase() > item2.getTitle().toLowerCase() ? -1 : 1;
    private static sortByNameDESC = (item1: WSItem, item2: WSItem) => this.sortByNameASC(item1, item2) * (-1);

    private static sortByLastModificationASC = (item1: WSItem, item2: WSItem) => item1.item.lastModificationTime > item2.item.lastModificationTime ? -1 : 1;
    private static sortByLastModificationDESC = (item1: WSItem, item2: WSItem) => this.sortByLastModificationASC(item1, item2) * (-1);

    private static folderFirst(item1: WSItem, item2: WSItem) {
        if (item1.isFolder() && !item2.isFolder()) return -1;
        if (!item1.isFolder() && item2.isFolder()) return 1;
        if (item1.getTitle().toLocaleLowerCase() > item2.getTitle().toLocaleLowerCase()) return -1;
        if (item1.getTitle().toLocaleLowerCase() < item2.getTitle().toLocaleLowerCase()) return 1;
        return 0;
    }

    private static sortBySizeASC(item1: WSItem, item2: WSItem) {
        if (item1.isFolder() && item2.isFile()) return 1;
        if (item1.isFile() && item2.isFolder()) return -1;
        if (item1.isFile() && item2.isFile()) {
            const sizeFile1 = item1.item.content ? item1.item.content.size : 0;
            const sizeFile2 = item2.item.content ? item2.item.content.size : 0;
            return sizeFile2 - sizeFile1;
        }
        return 0;
    }

    private static sortBySizeDESC = (item1: WSItem, item2: WSItem) => this.sortBySizeASC(item1, item2) * (-1);


    static getSortFunction(name: SortName, type: SortType): (a: WSItem, b: WSItem) => number {
        switch (name) {
            case SortName.Name:
                switch (type) {
                    case SortType.Asc: return this.sortByNameASC;
                    case SortType.Desc: return this.sortByNameDESC;
                }
            case SortName.LastModificationTime:
                switch (type) {
                    case SortType.Asc: return this.sortByLastModificationASC;
                    case SortType.Desc: return this.sortByLastModificationDESC;
                }
            case SortName.Size:
                switch (type) {
                    case SortType.Asc: return this.sortBySizeASC;
                    case SortType.Desc: return this.sortBySizeDESC;
                }
            case SortName.FolderFirst:
                return this.folderFirst;
        }
    }



}