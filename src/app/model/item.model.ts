import {AbstractItem, Content, LastAction } from "./model";

export class Item {
    
    private "@class": string;

    itemType : string | undefined;

    constructor(
        public name: string,
        public path: string,
        public parentId: string,
        public parentPath: String,
        public primaryType: String,
        public trashed: boolean,
        public externalManaged: boolean,
        public shared: boolean,
        public locked: boolean,
        public publicItem: boolean,
        public title: string,
        public description: string,
        public lastModifiedBy: string,
        public lastModificationTime: number,
        public creationTime: number,
        public lastAction: LastAction,
        public hidden: boolean,
        public accounting: null,
        public id: string,
        public owner: string, 
        public content?: Content | undefined,
        public externalRoot?: boolean | undefined,
        public privilege?: null | undefined,
        public vreFolder?: boolean | undefined,
        public displayName?: string | undefined) {
        }
      
} 
