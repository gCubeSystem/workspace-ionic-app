import { ItemWrapper } from "./item-wrapper.model";
import { Item } from "./item.model";


export class ItemList {

    constructor(
        public itemlist :Item[]
    ) {}
}