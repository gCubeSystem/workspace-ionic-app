import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { IonicModule } from '@ionic/angular';
import { WSItem } from '../model/ws-item';

@Component({
  selector: 'app-items-grid',
  templateUrl: './items-grid.component.html',
  styleUrls: ['./items-grid.component.scss'],
  imports: [CommonModule, IonicModule, MatIconModule]
})
export class ItemsGridComponent implements OnInit {

  @Output() actionSheetClickedEvent = new EventEmitter<WSItem>();
  @Output() itemClickedEvent = new EventEmitter<WSItem>();

  @Input() items: WSItem[] | undefined;
  @Input() underUploadItem: string[] = [];

  constructor() { }

  ngOnInit() {}

  itemClicked(item: WSItem){
    this.itemClickedEvent.emit(item);
  }

  actionsSheetClicked(item: WSItem){
    this.actionSheetClickedEvent.emit(item);
  }

}
