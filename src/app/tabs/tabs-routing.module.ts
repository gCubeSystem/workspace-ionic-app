import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'ws',
        pathMatch: 'full',
        loadChildren: () => import('../ws/ws.module').then(m => m.WsPageModule)
      },
      {
        path: 'ws/:folderId',
        pathMatch: 'full',
        loadChildren: () => import('../ws/ws.module').then(m => m.WsPageModule)
      },
      {
        path: 'vres',
        loadChildren: () => import('../vres/vres.module').then(m => m.VresPageModule)
      },
      {
        path: 'vres/:folderId',
        pathMatch: 'full',
        loadChildren: () => import('../vres/vres.module').then(m => m.VresPageModule)
      },
      {
        path: 'trash',
        loadChildren: () => import('../trash/trash.module').then(m => m.TrashPageModule)
      }
    ]
  },
  /*{
    path: '',
    redirectTo: '/tabs/ws',
    pathMatch: 'full'
  }*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
