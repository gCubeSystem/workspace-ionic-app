import { TestBed } from '@angular/core/testing';

import { D4sAuthService } from './d4sauth.service';

describe('D4sauthService', () => {
  let service: D4sAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(D4sAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
