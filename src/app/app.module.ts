import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { IonicModule, IonicRouteStrategy, ModalController, Platform, ToastController } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {initializeKeycloak} from './_helper/keycloak-init.factory'
import { D4sAuthService } from './d4sauth.service';
import { D4sIntentService } from './d4s-intent.service';
import {initializeIntent} from './_helper/intent-init.factory'
import { StoragehubService } from './storagehub.service';
import { EventService } from './event.service';


@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, HttpClientModule, KeycloakAngularModule],
  
  providers: [
    D4sAuthService, ModalController, StoragehubService, ToastController, EventService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
        provide: APP_INITIALIZER,
        useFactory: initializeKeycloak,
        multi: true,
        deps: [KeycloakService]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeIntent,
      multi: true,
      deps: [D4sIntentService]
  },
    
    ],
  bootstrap: [AppComponent]
})
export class AppModule {
  
  
  constructor(){}
  
}
