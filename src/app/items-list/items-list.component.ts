import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { IonicModule } from '@ionic/angular';
import { WSItem } from '../model/ws-item';
import { humanFileSize } from '../_helper/utils';

@Component({
  standalone: true,
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
  imports: [CommonModule, IonicModule, MatIconModule]
})
export class ItemsListComponent implements OnInit {

  @Output() actionSheetClickedEvent = new EventEmitter<WSItem>();
  @Output() itemClickedEvent = new EventEmitter<WSItem>();

  @Input() items: WSItem[] | undefined;
  @Input() underUploadItem: string[] = [];

  constructor() { }

  ngOnInit() {}

  itemClicked(item: WSItem){
    this.itemClickedEvent.emit(item);
  }

  actionsSheetClicked(item: WSItem){
    this.actionSheetClickedEvent.emit(item);
  }

  getFileSize(item: WSItem){
    if (item.isFile() && item.item?.content)
      return humanFileSize(item.item.content.size);
    return undefined;
  }
}
