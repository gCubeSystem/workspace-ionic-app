import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UploaderInfoService {

  myMap = new Map<string, string[]>();

  constructor() {
  }

  uploadStarted(itemId: string, fileName: string) {
    var values = this.myMap.get(itemId);
    if (!values)
      values = [fileName];
    else
      values.push(fileName);
    this.myMap.set(itemId, values);
  }

  uploadFinished(itemId: string , fileName: string) {
    var values = this.myMap.get(itemId);
    var newValues: string[] = [];
    values?.filter(val => val!= fileName).forEach(val => newValues?.push(val))
    if (newValues)
      if (newValues.length>0)
        this.myMap.set(itemId, newValues);
      else this.myMap.delete(itemId);
  }

  getUnderUpload(itemId: string): string[]{
    var values = this.myMap.get(itemId);
    //console.log(`get upload called with array ${values}`);
    return values? values : [];
  }

}
