import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { initialize } from '@ionic/core';
import { SplashScreen } from '@capacitor/splash-screen';
import { EventService } from './event.service';
import { App } from '@capacitor/app';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
   constructor(private platform: Platform, private event: EventService){
    this.initializeApp();
  }
  
   initializeApp(){
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(10, () => {
        this.event.BackButtonEvent.emit("{}");
      });
      SplashScreen.hide();
     })
   }
}
