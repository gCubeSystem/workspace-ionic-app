import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, retry, switchMap } from 'rxjs/operators';
import { HttpErrorResponse, HttpHeaders } from 'node-angular-http-client';
import { Item } from './model/item.model';
import { ItemList } from './model/itemlist.model';
import { ItemWrapper } from './model/item-wrapper.model';
import { D4sAuthService } from './d4sauth.service';
import { WSItem } from './model/ws-item';


const shURL ="https://api.dev.d4science.org/workspace"

@Injectable()
export class StoragehubService {
  constructor(private http: HttpClient, private auth : D4sAuthService) {
   }
 

  async getWsRoot() {
    const getWsURL = `${shURL}?exclude=hl:accounting`;
    const bearer = await this.auth.getSecureHeader();
    return this.http.get<ItemWrapper>(getWsURL, {headers: {"Authorization": bearer },  observe: 'body', responseType: 'json'}).pipe(
      map(value => value.item),
      catchError(this.error)
    );
  }
  
  async getTrashRoot(): Promise<Observable<Item>> {
    const bearer = await this.auth.getSecureHeader();
    const getTrashURL = `${shURL}/trash/?exclude=hl:accounting`;
    return this.http.get<ItemWrapper>(getTrashURL, {headers: {"Authorization": bearer }, observe: "body", responseType: "json"}).pipe(
      map(value => value.item),
      catchError(this.error)
    );
  }

  async getVres(): Promise<Observable<Item[]>> {
    const bearer = await this.auth.getSecureHeader();
    const getVresURL = `${shURL}/vrefolders/?exclude=hl:accounting`;
    return this.http.get<ItemList>(getVresURL, {headers: {"Authorization": bearer }, observe: 'body', responseType: 'json'}).pipe(
      map(value => value.itemlist),
      catchError(this.error)
    );
  }

  async getItem(id: string): Promise<Observable<Item>> {
    const bearer = await this.auth.getSecureHeader();
    const getWsURL = `${shURL}/items/${id}?exclude=hl:accounting`;
    return this.http.get<ItemWrapper>(getWsURL, {headers: {"Authorization": bearer }, observe: 'body', responseType: 'json'}).pipe(
      map(value => value.item),
      catchError(this.error)
    );
  }

  async getChildren(id:string, onlyFolder?:boolean): Promise<Observable<Item[]>>{
    const bearer = await this.auth.getSecureHeader();
    let getWsURL = `${shURL}/items/${id}/children?exclude=hl:accounting`;
    if (onlyFolder)
      getWsURL += '&onlyType=nthl:workspaceItem';
    return this.http.get<ItemList>(getWsURL, {headers: {"Authorization": bearer }}).pipe(
      map(values => values.itemlist),
      catchError(this.error)
    );
  }

  async getAnchestor(id:string): Promise<Observable<Item[]>>{
    const bearer = await this.auth.getSecureHeader();
    let getWsURL = `${shURL}/items/${id}/ancherstors?exclude=hl:accounting`;
    console.log("ws children "+getWsURL);
    return this.http.get<ItemList>(getWsURL, {headers: {"Authorization": bearer }, observe: 'body', responseType: 'json'}).pipe(
      map(value => value.itemlist),
      catchError(this.error)
    );
  }

  async uploadFile(parentId: string, name: string, file: File){
    const bearer = await this.auth.getSecureHeader();
    let uploadURL = `${shURL}/items/${parentId}/create/FILE`;
    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", "");
    formData.append("file",file);
    return this.http.post(uploadURL, formData, {headers: {"Authorization": bearer }, observe: 'events', responseType: 'text', reportProgress: true}).pipe(
      catchError(this.error)
    );
  }

  async downloadFile(itemId: string){
    const bearer = await this.auth.getSecureHeader();
    let downloadUrl = `${shURL}/items/${itemId}/download`;
    return this.http.get(downloadUrl, {headers: {"Authorization": bearer }, responseType: 'blob',  observe: 'body'}).pipe(
      catchError(this.error)
    );
  }
  
  async createFolder(parentId: string, name: string, description: string) {
    const bearer = await this.auth.getSecureHeader();
    let createFolderURL = `${shURL}/items/${parentId}/create/FOLDER`;
    const formData = new FormData();
    let body = `name=${name}&description=${description}`;
    return this.http.post(createFolderURL, body, {observe: 'body', responseType: 'text' , headers: { 'Content-Type' : 'application/x-www-form-urlencoded', "Authorization": bearer }}).pipe(
      catchError(this.error)
    );
  }

  async deleteItem(itemId: string) : Promise<Observable<any>> {
    const bearer = await this.auth.getSecureHeader();
    let deleteItemUrl = `${shURL}/items/${itemId}`;
    return this.http.delete( deleteItemUrl,  {headers: {"Authorization": bearer }}).pipe(
      catchError(this.error)
    );
  }

  async renameItem(itemId: string, newName: string) {
    const bearer = await this.auth.getSecureHeader();
    let renameItemUrl = `${shURL}/items/${itemId}/rename`;
    let body = `newName=${newName}`;
    return this.http.put(renameItemUrl,body,{observe: 'body', responseType: 'text' ,headers: { 'Content-Type' : 'application/x-www-form-urlencoded', "Authorization": bearer }}).pipe(
      catchError(this.error)
    );
  }

  async copyItem(destinationId: string, itemId: string, name:string) {
    const bearer = await this.auth.getSecureHeader();
    let copyItemUrl = `${shURL}/items/${itemId}/copy`;
    let body = `destinationId=${destinationId}&fileName=${name}`;
    return this.http.put(copyItemUrl,body,{observe: 'body', responseType: 'text' ,headers: { 'Content-Type' : 'application/x-www-form-urlencoded', "Authorization": bearer }}).pipe(
      catchError(this.error)
    );
  }

  async moveItem(destinationId: string, itemId: string)  {
    const bearer = await this.auth.getSecureHeader();
    let copyItemUrl = `${shURL}/items/${itemId}/move`;
    let body = `destinationId=${destinationId}`;
    return this.http.put(copyItemUrl,body,{observe: 'body', responseType: 'text' ,headers: { 'Content-Type' : 'application/x-www-form-urlencoded', "Authorization": bearer }}).pipe(
      catchError(this.error)
    );
  }

  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }

}
