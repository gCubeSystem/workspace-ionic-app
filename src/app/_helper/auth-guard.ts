import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { D4sAuthService } from '../d4sauth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(
    private auth: D4sAuthService,
    private router: Router
  ) {
  }
  
  canActivate() {
    
    console.log("in the authguard");    

    var logged = this.auth.isAuthorized();    
    
    if (!logged) {
        this.router.navigate(['login']);
    }

    console.log(`is logged in ? ${logged}`);

    return logged;
  }
}