import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { D4sAuthService } from '../d4sauth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  
  constructor(
    private auth: D4sAuthService,
    private router: Router
  ) {
  }
  
  async canActivate() {
    
    var logged =this.auth.isAuthorized();    
        
    if (logged) {
        this.router.navigate(['tabs','ws']);
    }

    return !logged;
  }
}