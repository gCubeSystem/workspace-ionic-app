import { ShareExtension } from "capacitor-share-extension";
import { D4sIntentService } from "../d4s-intent.service";

export function initializeIntent(d4sIntent: D4sIntentService) {

    return () => {
        window.addEventListener('sendIntentReceived', () => {
            console.log("event fired");
            d4sIntent.checkIntent();
        });
        d4sIntent.checkIntent();
    }

}

