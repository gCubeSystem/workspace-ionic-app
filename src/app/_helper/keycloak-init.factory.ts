import { KeycloakService } from "keycloak-angular";
import * as Keycloak from "keycloak-js";
export function initializeKeycloak(keycloak: KeycloakService) {

    return () => keycloak.init({
        config: {
            url: 'https://accounts.dev.d4science.org/auth',
            realm: 'd4science',
            clientId: 'workspaceapp',
        },
        initOptions: {
            redirectUri: 'd4sworkspace://redirect', //'http://localhost:8100', // 
            checkLoginIframe: false
        },
        shouldAddToken: (request) => false
    }).then((res) => console.log(res))
        .catch((err) => console.error(err));
}