import { TestBed } from '@angular/core/testing';

import { StoragehubService } from './storagehub.service';

describe('StoragehubService', () => {
  let service: StoragehubService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoragehubService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
