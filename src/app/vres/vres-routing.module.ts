import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VresPage } from './vres.page';

const routes: Routes = [
  {
    path: '',
    component: VresPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VresPageRoutingModule {}
