import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { VresPage } from './vres.page';

describe('VresPage', () => {
  let component: VresPage;
  let fixture: ComponentFixture<VresPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VresPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(VresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
