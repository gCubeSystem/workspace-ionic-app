import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VresPage } from './vres.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { VresPageRoutingModule } from './vres-routing.module';
import { ShowFolderComponent } from '../show-folder/show-folder.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    VresPageRoutingModule,
    ShowFolderComponent
  ],
  declarations: [VresPage]
})
export class VresPageModule {}
