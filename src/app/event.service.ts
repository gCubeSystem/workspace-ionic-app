import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private onGoBack : Function =  ()=>{}

  ReloadEvent = new EventEmitter<string>();

  BackButtonEvent = new EventEmitter<any>();

  constructor() { }
  
}

