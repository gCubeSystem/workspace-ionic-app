import { AfterViewInit, Component, } from '@angular/core';
import { Router } from '@angular/router';
import { D4sAuthService } from '../d4sauth.service';


declare var cordova:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements AfterViewInit {

  constructor(private auth:  D4sAuthService, private router : Router) { }

  ngAfterViewInit(): void{
     this.auth.login().then(ret => {
        console.log("logged in - trying to navigates to tabs "+ret);
        this.router.navigateByUrl('/tabs/ws').then(() => console.log("navigating")).catch((e) => console.log("error "+e));
      }).catch( err => console.log(err));
   }

  
}
