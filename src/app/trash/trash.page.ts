import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { lastValueFrom } from 'rxjs';
import { Item } from '../model/item.model';
import { WSItem } from '../model/ws-item';
import { StoragehubService } from '../storagehub.service';
import { presentConnectionAlert } from '../_helper/utils';

@Component({
  selector: 'app-trash',
  templateUrl: 'trash.page.html',
  styleUrls: ['trash.page.scss'],
  providers: [StoragehubService],
})


export class TrashPage implements OnInit {

  values: WSItem[] | undefined;

  currentItem : WSItem | undefined ; 

  constructor(private storagehub: StoragehubService, private alertCtrl: AlertController) { }


  ngOnInit() {
    this.storagehub.getTrashRoot().then((obs) => obs.subscribe({
      next: (res) => this.onSuccess(res),
      error: err => presentConnectionAlert(err, this.alertCtrl)
    }
    ));
  }

  private onSuccess(res: Item) {
    this.currentItem = new WSItem(res);
    this.storagehub.getChildren(res.id, false).then((obs) => obs.subscribe({
      next: (res) => {
        const tmpItems$: WSItem[] = []
        res.forEach(i => tmpItems$.push(new WSItem(i)));
        this.values = tmpItems$;
      },
      error: err => presentConnectionAlert(err, this.alertCtrl)
    }
    ))
  }

  public getValues(): WSItem[] | undefined{
    return this.values;
  }

  public getCurrentItem(){
    return this.currentItem;
  }
}
