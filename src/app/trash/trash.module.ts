import { IonicModule } from '@ionic/angular';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TrashPage } from './trash.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { TrashPageRoutingModule } from './trash-routing.module';
import { ShowFolderComponent } from '../show-folder/show-folder.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    TrashPageRoutingModule,
    ShowFolderComponent
    
  ],
  declarations: [TrashPage]
})
export class TrashPageModule {}
