import { TestBed } from '@angular/core/testing';

import { UploaderInfoService } from './uploader-info.service';

describe('UploaderInfoService', () => {
  let service: UploaderInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploaderInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
