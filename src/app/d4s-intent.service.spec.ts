import { TestBed } from '@angular/core/testing';

import { D4sIntentService } from './d4s-intent.service';

describe('D4sIntentService', () => {
  let service: D4sIntentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(D4sIntentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
