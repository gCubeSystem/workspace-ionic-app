import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_helper/auth-guard';

const loginModule = () => import('./login/login.module').then(m => m.LoginPageModule);

const routes: Routes = [
 
  {
    path: 'login',
    loadChildren: loginModule, // canActivate : [LoginGuard]
  },
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule) , canActivate: [AuthGuard]
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
