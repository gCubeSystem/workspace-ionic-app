import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { WsPage } from './ws.page';

describe('WsPage', () => {
  let component: WsPage;
  let fixture: ComponentFixture<WsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WsPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(WsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
