import { IonicModule} from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WsPage } from './ws.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { WsPageRoutingModule } from './ws-routing.module';
import { ShowFolderComponent } from '../show-folder/show-folder.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    WsPageRoutingModule,
    ShowFolderComponent,
  ],
  declarations: [WsPage]
})
export class WsPageModule {}
