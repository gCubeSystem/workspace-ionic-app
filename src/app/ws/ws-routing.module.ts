import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WsPage } from './ws.page';

const routes: Routes = [
  {
    path: '', 
    component: WsPage
   }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WsPageRoutingModule {}
