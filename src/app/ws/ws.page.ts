import { Component, OnInit } from '@angular/core';
import { AlertController} from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { StoragehubService } from '../storagehub.service';
import { WSItem } from '../model/ws-item';
import { Item } from '../model/item.model';
import { presentConnectionAlert } from '../_helper/utils';

@Component({
  selector: 'app-ws',
  providers: [StoragehubService],
  templateUrl: 'ws.page.html',
  styleUrls: ['ws.page.scss'],
})
export class WsPage implements OnInit {

  values: WSItem[] | undefined = undefined;

  item: WSItem | undefined;

  root: boolean = false;

  private static cashItems: WSItem[] = [];

  constructor(private storagehub: StoragehubService,
    private router: Router,
    private route: ActivatedRoute,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    var folderId: string | undefined = this.route.snapshot.paramMap.get('folderId') || undefined;
    var tmpItem: WSItem | undefined = undefined;
    if (folderId)
      tmpItem = WsPage.cashItems.find((value) => value.item.id === folderId);

    this.root = !folderId;
    if (!tmpItem) {
      if (!folderId)
        this.storagehub.getWsRoot().then( (obs) => obs.subscribe({
          next: (res) => {
            this.item = new WSItem(res);
            this.onSuccess(res)
          },
          error: (err) => presentConnectionAlert(err, this.alertCtrl)
        }
        ))
      else //folder is not cached
        this.storagehub.getItem(folderId).then( (obs) => obs.subscribe({
          next: (res) => {
            this.item = new WSItem(res);
            this.onSuccess(res)
          },
          error: err => presentConnectionAlert(err, this.alertCtrl)
        }
        ))
    } else {
      this.item = tmpItem;
      this.onSuccess(tmpItem.item);
    }

  }

  private onSuccess(item: Item) {

    this.storagehub.getChildren(item.id, false).then( (obs) => obs.subscribe({
      next: (res) => {
        const tmpItems$: WSItem[] = []
        res.forEach(i => tmpItems$.push(new WSItem(i)));
        this.values = tmpItems$;
      },
      error: (err) => presentConnectionAlert(err, this.alertCtrl),
    })
    )
  }

  public getValues(): WSItem[] | undefined {
    return this.values;
  }

  openFolder(item: WSItem) {
    if (this.values)
      WsPage.cashItems = this.values;

    this.router.navigateByUrl(`tabs/ws/${item.item.id}`);
  }

  public getCurrentItem(): WSItem | undefined {
    return this.item;
  }

  getTitle() {
    return this.root ? "My Workspace" : this.item?.getTitle()
  }

  enableCreateOperations() : boolean {
    return this.getCurrentItem() != undefined;
  }  

}
