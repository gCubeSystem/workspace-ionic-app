import { Injectable } from '@angular/core';
import { App } from '@capacitor/app';
import { isPlatform, LoadingController, ModalController, ModalOptions, ToastController } from '@ionic/angular';
import { ShareExtension } from 'capacitor-share-extension';
import { EventService } from './event.service';
import { UploadFile } from './model/actions/upload-file';
import { StoragehubService } from './storagehub.service';
import { UploaderInfoService } from './uploader-info.service';

@Injectable({
  providedIn: 'root'
})
export class D4sIntentService {

  constructor(private modalCtrl: ModalController,
    private toastController: ToastController,
    private storagehub: StoragehubService,
    private uploaderInfo: UploaderInfoService,
    private event: EventService,
    private loadingCtrl: LoadingController) { }


  async checkIntent() {
    try {
      const result: any = await ShareExtension.checkSendIntentReceived();
      /* sample result::
      { payload: [
          {
              "type":"image%2Fjpg",
              "description":"",
              "title":"IMG_0002.JPG",
              // url contains a full, platform-specific file URL that can be read later using the Filsystem API.
              "url":"file%3A%2F%2F%2FUsers%2Fcalvinho%2FLibrary%2FDeveloper%2FCoreSimulator%2FDevices%2FE4C13502-3A0B-4DF4-98ED-9F31DDF03672%2Fdata%2FContainers%2FShared%2FAppGroup%2FF41DC1F5-54D7-4EC5-9785-5248BAE06588%2FIMG_0002.JPG",
              // webPath returns a path that can be used to set the src attribute of an image for efficient loading and rendering.
              "webPath":"capacitor%3A%2F%2Flocalhost%2F_capacitor_file_%2FUsers%2Fcalvinho%2FLibrary%2FDeveloper%2FCoreSimulator%2FDevices%2FE4C13502-3A0B-4DF4-98ED-9F31DDF03672%2Fdata%2FContainers%2FShared%2FAppGroup%2FF41DC1F5-54D7-4EC5-9785-5248BAE06588%2FIMG_0002.JPG",
          }]
       } 
       */
      if (result && result.payload && result.payload.length) {
        console.log('Intent received: ', JSON.stringify(result.payload[0]));
        var modalOptions: ModalOptions = new UploadFile().getModalOptions(this.storagehub, this.uploaderInfo, this.loadingCtrl, result.payload[0],
          (id: string) => this.event.ReloadEvent.emit(id), (text: string) => this.presentToast(text), this.shareExtensionClose);
        let modal = await this.modalCtrl.create(modalOptions);
        await modal.present();

      } else {
        console.log("nothing received");
      }
    } catch (err) {
      console.log(err);
    }
  }

  async presentToast(text: string) {
    const toast = await this.toastController.create({
      message: text,
      duration: 1500
    });
    await toast.present();
  }


  async shareExtensionClose() {
    ShareExtension.finish();
  }

}
