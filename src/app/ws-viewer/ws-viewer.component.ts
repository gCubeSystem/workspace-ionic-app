import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { IonicModule, ModalController } from '@ionic/angular';
import { EventService } from '../event.service';
import { Sorting, SortName, SortType } from '../model/sorting';
import { WSItem } from '../model/ws-item';
import { StoragehubService } from '../storagehub.service';

@Component({
  standalone: true,
  selector: 'app-ws-viewer',
  providers: [StoragehubService],
  templateUrl: './ws-viewer.component.html',
  styleUrls: ['./ws-viewer.component.scss'],
  imports: [
    CommonModule, IonicModule, MatIconModule
  ]
})
export class WsViewerComponent implements OnInit {

  constructor(private storagehub: StoragehubService, 
    private modalCtrl: ModalController, private eventService: EventService ) { }

  @Input() notClickableIds: string[] = [];
  @Input() notSelectableIds: string[] = [];
  @Input() finishLabel: string = "Confirm";
  @Input() title: string = "Operation";

  @Input() onSelected :Function = () => {};

  currentItem: WSItem | undefined;

  anchestors: WSItem[] | undefined ;

  items: WSItem[] | undefined = undefined;

  
  ngOnInit() {
     this.loadDocuments();
  }
  

  loadDocuments() {
    if (this.currentItem) {
      this.storagehub.getChildren(this.currentItem.item.id).then( (obs) => obs.subscribe(
        (res) => {
          const tmpItems$: WSItem[] = []
          res.forEach(i => tmpItems$.push(new WSItem(i)));
          this.items = tmpItems$.sort(Sorting.getSortFunction(SortName.FolderFirst, SortType.Asc));
        })
        );
    } else
      this.storagehub.getWsRoot().then( (obs) => obs.subscribe(
        (res) => this.addVresToWs(new WSItem(res))
      ));
  }

  addVresToWs(wsRoot: WSItem) {
    this.storagehub.getVres().then( (obs) => obs.subscribe(
      (res) => {
        const tmpItems$: WSItem[] = [wsRoot]
        res.forEach(i => tmpItems$.push(new WSItem(i)));
        this.items = tmpItems$;
      }));
  }

  isCurrentItemSelectable(){
    return this.currentItem && !this.notSelectableIds.includes(this.currentItem.item.id);
  }

  isItemClickable(item: WSItem){
    return item.isFolder() && !this.notClickableIds.includes(item.item.id); 
  }

  itemClicked(item: WSItem) {
    this.items = undefined;
    if (this.currentItem)
      this.anchestors?.push(this.currentItem);
    else this.anchestors =[]
    this.currentItem = item;
    this.ngOnInit();
  }

  backClicked() {
    this.items = undefined;
    if (this.anchestors?.length == 0){
      this.anchestors = undefined;
      this.currentItem = undefined;
    } else {
      var tmpitem = this.anchestors?.pop();
      this.currentItem = tmpitem;
    }
    this.loadDocuments();
  }

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    this.onSelected(this.currentItem);
    return this.modalCtrl.dismiss(null, 'confirm');
  }

  
}
